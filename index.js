const datasetA = {
	AA001: [
		{
			'6': 1,
			'7': 2,
			'8': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3,
			'7-': 4
		}
	],
	AA002: [
		{
			'6': 1,
			'7': 2,
			'8': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3,
			'7-': 4
		}
	],
	AA004: [
		{
			'6': 1,
			'7': 2,
			'8': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3
		},
		{
			'6': 1,
			'6-': 2,
			'7': 3,
			'7-': 4
		}
	]
}

const datasetB = {
	AA001: [
		{
			'5': 0,
			'5-': 0,
			'6': 0,
			'6-': 0,
			'7': 0,
			'8': 0,
			AAA: 0
		},
		{
			'7': 0,
			'8': 0,
			'7-': 0,
			AA: 0
		},
		{
			'8-': 0,
			AA: 0
		}
	],
	AA003: [
		{
			'5': 0,
			'5-': 0,
			'6': 0,
			'6-': 0,
			'7': 0,
			'8': 0,
			AAA: 0
		},
		{
			AA: 0,
			'7-': 0,
			'7': 0,
			'8': 0
		},
		{
			'8-': 0,
			AA: 0
		}
	],
	AA004: [
		{
			'5': 0,
			'5-': 0,
			'6': 0,
			'6-': 0,
			'7': 0,
			'8': 0,
			AAA: 0
		},
		{
			'7': 0,
			'8': 0,
			'7-': 0,
			AA: 0
		},
		{
			'8-': 0,
			AA: 0
		}
	],
	AA005: [
		{
			'5': 0,
			'5-': 0,
			'6': 0,
			'6-': 0,
			'7': 0,
			'8': 0,
			AAA: 0
		},
		{
			AA: 0,
			'7-': 0,
			'7': 0,
			'8': 0
		},
		{
			'8-': 0,
			AA: 0
		}
	]
}

const shift = (A, B) => {
	const sizeA = Object.keys(A || {})
	const sizeB = Object.keys(B || {})
	// console.log(sizeB)
	let match = true
	let res = {}
	sizeA.map(sizeA => {
		if (sizeB.indexOf(sizeA) == -1) match = false
	})
	if (match) {
		sizeA.map(sizeA => {
			res[sizeA] = A[sizeA]
		})
	} else {
		sizeA.map((sizeA, index) => {
			if (sizeB[index] !== undefined) res[sizeB[index]] = A[sizeA]
		})
	}
	return res
}

const paginate = (A, B) => {
	let res = []
	A.map((a, index) => {
		res.push(shift(A[index], B[index]))
	})
	return res
}

const keyA = Object.keys(datasetA)
const keyB = Object.keys(datasetB)

// console.log(paginate(datasetA['AA001'], datasetB['AA001']))
let datasetC = {}

keyB.map(keyB => {
	if (keyA.indexOf(keyB) != -1) datasetC[keyB] = paginate(datasetA[keyB], datasetB[keyB])
	// else if (keyB.indexOf(keyA) == -1) console.log(`Removed ${keyB}`)
	else if (keyA.indexOf(keyB) == -1) datasetC[keyB] = datasetB[keyB]
})
keyA.map(keyA => {
	if (keyB.indexOf(keyA) == -1) console.log(`Removed ${keyA}`)
})

console.log(datasetC)

// If old == new, shift
// If old exist, new not exist == delete old, show removed id
// If old not exist, new exist, replaced with new
